

# REDO ALL WITH HG19 

bedtools intersect -a CN_pol2.narrowPeak -b iAATF_pol2.narrowPeak -v   > siControl_specific.bed
bedtools intersect -a CN_pol2.narrowPeak -b iAATF_pol2.narrowPeak  -u   > siControl_siChe-1_common.bed
bedtools intersect -b CN_pol2.narrowPeak -a iAATF_pol2.narrowPeak -v   > siChe-1_specific.bed


# for peak_set in *bed;
# do
# 	annotatePeaks.pl $peak_set hg19 -annStats $peak_set.stats.tsv > $peak_set.hg19.homer.tsv
# done;


# merge annotation to compare... (R)


siche1 = read.csv("siChe-1_specific.bed.stats.tsv", sep="\t", header=T)[ 1:13, ]
common = read.csv("siControl_siChe-1_common.bed.stats.tsv", sep="\t", header=T)[ 1:13, ]
sicontrol = read.csv("siControl_specific.bed.stats.tsv", sep="\t", header=T)[ 1:13, ]

ab = merge(siche1, common,by=c("Annotation"), suffixes=c(".siChe-1",".Common"))

abc = merge(ab, sicontrol,by=c("Annotation"), suffixes=c("dadada",".siControl"))

rownames(abc) = abc$Annotation

final = abc [ , grepl(names(abc), pattern="Ratio")]

write.table(final, "final_ratio_genomic.tsv", col.names=T, row.names=F, quote=F)

# extract promoter-specific genes for each class

# why did I do it in SH? Don't remember

# for i in *homer.tsv;
# do 
# 	echo $i;
# 	grep "promoter-TSS" $i  | cut -f 16 | sort | uniq > $i.promoter.genes.tsv
# done;


che1_genes = read.csv("siChe-1_specific.bed.hg19.homer.tsv.promoter.genes.tsv", sep="\t",header=F)[,1]
common_genes = read.csv("siControl_siChe-1_common.bed.hg19.homer.tsv.promoter.genes.tsv", sep="\t",header=F)[,1]
sicontrol_genes = read.csv("siControl_specific.bed.hg19.homer.tsv.promoter.genes.tsv", sep="\t",header=F)[,1]


che1_specific_genes = setdiff(setdiff(che1_genes, common_genes), sicontrol_genes)

write.table(che1_specific_genes, "siche1_only_genes_promoters.txt", col.names=F, row.names=F, quote=F)


