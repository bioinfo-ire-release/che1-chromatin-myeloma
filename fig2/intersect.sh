

bedtools intersect -a CN_pol2.narrowPeak -b iAATF_pol2.narrowPeak -v   > siControl_specific.bed
bedtools intersect -a CN_pol2.narrowPeak -b iAATF_pol2.narrowPeak  -u   > siControl_siChe-1_common.bed
bedtools intersect -b CN_pol2.narrowPeak -a iAATF_pol2.narrowPeak -v   > siChe-1_specific.bed

for peak_set in *bed;
do
	annotatePeaks.pl $peak_set hg38 -annStats $peak_set.stats.tsv > $peak_set.homer.tsv
done;

